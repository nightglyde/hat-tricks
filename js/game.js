var canvas, context, w, h, scaleX, scaleY,
    upperLimit, lowerLimit, leftLimit, rightLimit,
    world, floor, RWall, ceiling, LWall,
    mouse, mouseConstraint, mouseActive,
    hats, stars, starConstraints,
    magicPoints, magicThreshold, maxStars, maxMagic,
    requestId;

// convert degrees to radians
function radians (degrees) {return degrees * Math.PI / 180;}

// random number such that lower <= n < upper
function randomInt (lower, upper) {
    return lower + Math.floor(Math.random() * (upper-lower));
}

// turn decimal into 2-digit hexadecimal
var hex = "0123456789ABCDEF".split("");
function getHex (d) {
    return hex[Math.floor(d / 16)] + hex[Math.floor(d % 16)];
}

// colour profiles
var ALL = 0;
var HAT = 1;
var BAND = 2;
var STAR = 3;
var TEXT = 4;

// generate a random colour
function generateColour (type) {
    type = type || ALL;

    var R = randomInt(0, 64);
    var G = randomInt(0, 64);
    var B = randomInt(0, 64);

    // different levels of brightness
    var points;
    if (type == ALL) {points = randomInt(0, 10);}
    else if (type == HAT) {points =  randomInt(0, 3);}
    else if (type == BAND) {points =  randomInt(2, 5);}
    else if (type == STAR) {points = randomInt(6, 8);}
    else if (type == TEXT) {points = 5;}

    for (var i=0; i<points; i++) {
        selected = randomInt(0, 3);
        if (selected == 0) {R += 64;}
        else if (selected == 1) {G += 64;}
        else if (selected == 2) {B += 64;}
    }

    // if one of the colour components has greater than 255, share the excess
    // between the other colour components
    var overflow;
    overflow = Math.max(R-255, 0) + Math.max(G-255, 0) + Math.max(B-255, 0);
    R = Math.min(R, 255);
    G = Math.min(G, 255);
    B = Math.min(B, 255);
    while (overflow > 0) {
        if (R < 255 && G < 255) {
            R += overflow/2;
            G += overflow/2;
        } else if (R < 255 && B < 255) {
            R += overflow/2;
            B += overflow/2;
        } else if (G < 255 && B < 255) {
            G += overflow/2;
            B += overflow/2;
        } else if (R < 255) {
            R += overflow;
        } else if (G < 255) {
            G += overflow;
        } else if (B < 255) {
            B += overflow;
        }
        overflow = Math.max(R-255, 0) + Math.max(G-255, 0) + Math.max(B-255, 0);
        R = Math.min(R, 255);
        G = Math.min(G, 255);
        B = Math.min(B, 255);
    }

    // build hexadecimal string
    var colour = "#";
    colour += getHex(Math.floor(R));
    colour += getHex(Math.floor(G));
    colour += getHex(Math.floor(B));
    return colour;
}

// turn mouse coords into world coords
function getMouseCoords (event) {
        var rect = canvas.getBoundingClientRect();
        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;
        x = (x - w / 2) / scaleX;
        y = (y - h / 2) / scaleY;
        return [x, y];
}

// mouse object, which allows the player to interact with physics objects
function createMouse () {
    mouse = new p2.Body();
    world.addBody(mouse);

    // mouse click
    canvas.addEventListener("mousedown", function (event) {
        mouseActive = true;

        var position = getMouseCoords(event);
        mouse.position[0] = position[0];
        mouse.position[1] = position[1];

        //grab object
        var hatsClicked = world.hitTest(position, hats);
        if (hatsClicked.length > 0) {
            if (mouseConstraint) {
                world.removeConstraint(mouseConstraint);
            }
            mouseConstraint = new p2.RevoluteConstraint(
                mouse, hatsClicked[0], {
                worldPivot: position,
                collideConnected: false
            });
            world.addConstraint(mouseConstraint);
        } else {
            // magic tricks
            if (magicPoints >= magicThreshold) {
                magicPoints -= magicThreshold;
                var selector = randomInt(0, 10);
                if (selector < 5) {
                    // create hat
                    createHat(position);
                    starBurst(position, randomInt(2, 7));
                    maxMagic += magicThreshold;
                    magicThreshold += 10;
                } else {
                    // create burst of stars
                    var lower = Math.floor(magicThreshold*0.75);
                    var upper = Math.floor(magicThreshold*1.125);
                    starBurst(position, randomInt(lower, upper));
                }
            }
        }
    });

    // mouse movement
    canvas.addEventListener("mousemove", function (event) {
        mouseActive = true;
        var position = getMouseCoords(event);
        mouse.position[0] = position[0];
        mouse.position[1] = position[1];
    });

    // release mouse click
    canvas.addEventListener("mouseup", function (event) {
        mouseActive = true;

        world.removeConstraint(mouseConstraint);
        mouseConstraint = null;
    });

    // cursor goes off edge of canvas
    canvas.addEventListener("mouseout", function (event) {
        mouseActive = false;

        world.removeConstraint(mouseConstraint);
        mouseConstraint = null;
    });
}

var hatPolygon = [
    [-5, -9.5], [0, -10], [5, -9.5],
    [9, -8.5],    [12, -6.5],    [15, -2.5],
    [13, -1], [11, -4],  [8, -6],
    [8, -1.5],    [9, 6],      [10, 10], 
    [-10, 10],   [-9, 6],     [-8, -1.5],
    [-8, -6], [-11, -4], [-13, -1],
    [-15, -2.5],  [-12, -6.5],   [-9, -8.5]
];

function createHat (position) {
    // create hat
    hat = new p2.Body({mass: 10, position: position});
    hat.fromPolygon(hatPolygon);
    world.addBody(hat);
    hats.push(hat);

    // add extra properties
    hat.colour = generateColour(HAT);
    hat.bandColour = generateColour(HAT);
    hat.magic = false;
}

function drawHat (hat) {
    var x = hat.position[0],
        y = hat.position[1];

    context.save();
    context.translate(x, y);
    context.rotate(hat.angle);

    // draw outside of hat
    context.beginPath();
    var point = hatPolygon[0];
    context.moveTo(point[0], point[1]);
    for (var i=1; i<hatPolygon.length; i++) {
        point = hatPolygon[i];
        context.lineTo(point[0], point[1]);
    }
    context.closePath();
    context.fillStyle = hat.colour;
    context.fill();
    context.stroke();

    // draw hat band
    context.beginPath();
    context.moveTo(8, -6); context.lineTo(4, -7);
    context.lineTo(0, -7.5);
    context.lineTo(-4, -7); context.lineTo(-8, -6);
    context.lineTo(-8, -2); context.lineTo(-4, -3);
    context.lineTo(0, -3.5);
    context.lineTo(4, -3); context.lineTo(8, -2);
    context.closePath();
    context.fillStyle = hat.bandColour;
    context.fill();
    context.stroke();

    context.restore();
}

function createStar (position) {
    if (stars.length >= maxStars) {
        return;
    }

    // generate random star
    var numPoints = randomInt(5, 11);
    var angle = Math.PI / numPoints;
    var outer = Math.random() + 2;
    var inner = 1;

    // generate points of star
    var polygon = [];
    var currAngle, x, y; 
    for (var i=0; i<numPoints; i++) {
        currAngle = 2*i * angle;
        x = Math.sin(currAngle) * outer;
        y = -Math.cos(currAngle) * outer;
        polygon.push([x, y]);

        currAngle = (1+2*i) * angle;
        x = Math.sin(currAngle) * inner;
        y = -Math.cos(currAngle) * inner;
        polygon.push([x, y]);
    }

    // create star
    star = new p2.Body({mass: 0.1, position: position});
    star.fromPolygon(polygon);
    world.addBody(star);
    stars.push(star);

    // add extra properties
    star.polygon = polygon;
    star.colour = generateColour(STAR);

    // attract to mouse object
    starConstraint = new p2.DistanceConstraint(star, mouse, {
        distance: 0,
        maxForce: 5
    });
    world.addConstraint(starConstraint);
    starConstraints.push(starConstraint);
}

// create a bunch of stars around the mouse
function starBurst (position, numStars) {
    var angle = 2*Math.PI / numStars;
    var x, y;
    var currAngle = Math.random() * angle;
    for (var i=0; i<numStars; i++) {
        currAngle += angle
        x = position[0] + Math.sin(currAngle) * randomInt(5, 21);
        y = position[1] - Math.cos(currAngle) * randomInt(5, 21);
        x = Math.max(Math.min(x, rightLimit), leftLimit);
        y = Math.max(Math.min(y, upperLimit), lowerLimit);
        createStar([x, y]);
    }
}

// collect magic points when stars touch mouse object
function collectStar (star) {
    if (magicPoints >= maxMagic) {
        return;
    }

    magicPoints++;

    // remove star from world
    var i = stars.indexOf(star);
    world.removeConstraint(starConstraints[i])
    world.removeBody(star);

    // remove star from data structures
    starConstraints.splice(i, 1);
    stars.splice(i, 1);
}

function drawStar (star) {
    var x = star.position[0],
        y = star.position[1];

    context.save();
    context.translate(x, y);
    context.rotate(star.angle);

    // draw outside of star
    context.beginPath();
    var point = star.polygon[0];
    context.moveTo(point[0], point[1]);
    for (var i=1; i<star.polygon.length; i++) {
        point = star.polygon[i];
        context.lineTo(point[0], point[1]);
    }
    context.closePath();

    context.fillStyle = star.colour;
    context.fill();
    context.stroke();

    context.restore();
}

// draw score at top of canvas
function drawScore () {
    context.save();

    context.textAlign = "left";
    context.fillText("Hats: " + hats.length, 20, 30);

    context.textAlign = "centre";
    context.fillText("Magic: " + magicPoints + "/" + maxMagic, 250, 30);

    context.textAlign = "right";
    context.fillText("Next hat: " + magicThreshold, 580, 30);

    context.restore();
};

function render () {
    context.clearRect(0, 0, w, h);

    context.save();
    context.translate(w/2, h/2);
    context.scale(scaleX, scaleY);

    // draw hats
    for (var i=0; i<hats.length; i++) {
        drawHat(hats[i]);
    }

    // draw stars
    for (var i=0; i<stars.length; i++) {
        drawStar(stars[i]);
    }

    context.restore();

    drawScore();
}

// animation loop
function animate () {
    requestId = requestAnimationFrame(animate);
    world.step(1/60);
    render();
}

// initialise and start game
function startGame () {
    if (!requestId) {
        canvas = document.getElementById("canvas");
        context = canvas.getContext("2d");
        context.lineWidth = 0.2;
        context.font = "20px Serif";

        // initialise dimensions
        w = canvas.width;
        h = canvas.height;
        scaleX = 5;
        scaleY = -5;

        upperLimit = -(h/scaleY)/2;
        lowerLimit = (h/scaleY)/2;
        leftLimit = -(w/scaleX)/2;
        rightLimit = (w/scaleX)/2;

        // create world for physics objects
        world = new p2.World();

        // create boundaries
        ceiling = new p2.Body({position: [0, upperLimit], angle: radians(180)});
        ceiling.addShape(new p2.Plane());
        world.addBody(ceiling);

        floor = new p2.Body({position: [0, lowerLimit], angle: radians(0)});
        floor.addShape(new p2.Plane());
        world.addBody(floor);

        leftWall = new p2.Body({position: [leftLimit, 0], angle: radians(270)});
        leftWall.addShape(new p2.Plane());
        world.addBody(leftWall);

        rightWall = new p2.Body({position: [rightLimit, 0], angle: radians(90)});
        rightWall.addShape(new p2.Plane());
        world.addBody(rightWall);

        // initialise values
        hats = [];
        stars = [];
        starConstraints = [];
        magicPoints = 0;
        magicThreshold = 10;
        maxStars = 50;
        maxMagic = 10;

        // create starting objects
        createMouse();
        createHat([0, 0]);

        // more actions
        world.on("postStep", function (event) {
            // mouse cursor collects stars
            if (mouseActive) {
                var starsCollected = world.hitTest(mouse.position, stars);
                var star;
                for (var i=0; i<starsCollected.length; i++) {
                    star = starsCollected[0];
                    collectStar(star);
                }
            }

            // check hats for magic
            var hat, angle;
            var position = [0, 0];
            for (var i=0; i<hats.length; i++) {
                hat = hats[i];
                angle = hat.angle % (2*Math.PI);
                if (angle < 0) {
                    angle += (2*Math.PI);
                }
                if (hat.magic) {
                    if (Math.PI-0.5 < angle && angle < Math.PI+0.5) {
                        // hat is upside down
                        position[0] = hat.position[0] + 10*Math.sin(angle);
                        position[1] = hat.position[1] - 10*Math.cos(angle);
                        var newStars = randomInt(1, 4);
                        for (var i=0; i<newStars; i++) {
                            createStar(position);
                        }
                        hat.magic = false;
                    }
                } else {
                    if (-0.5 < angle && angle < 0.5) {
                        // hat is right-side up
                        hat.magic = true;
                    }
                }
            }
        });

        animate();
    }
}

// changed to different page of website
function stopGame () {
    if (requestId) {
        window.cancelAnimationFrame(requestId);
        requestId = undefined;
        world.clear();
    }
}

