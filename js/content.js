var content, page;

var INSTRUCTIONS = 0,
    GAME  = 1,
    HAT   = 2,
    ABOUT = 3;

// html content of each page
var instructionsContent = "<h2>Instructions</h2><article><ul>" +
                          "<li>You are a collector of magical hats.</li>" +
                          "<li>You start the game with one hat. Your goal is to" +
                          " get as many hats as you can.</li>" +
                          "<li>Magical hats produce magic if used" +
                          " correctly.</li>" +
                          "<li>You can move a hat around the canvas by" +
                          " clicking and dragging with your mouse.</li>" +
                          "<li>Once you have enough magic, you can do a magic" +
                          " trick. Since you are a magical hat collector, you" +
                          " want to create more hats. However, your magic is a" +
                          " bit unreliable.</li>" +
                          "<li>Once you have enough magic, click on an empty" +
                          " part of the canvas to try to conjure a new" +
                          " hat.</ul></article>";

var gameContent = "<h2>Play Game</h2><article>" +
                  "<canvas id=\"canvas\" width =\"600\" height=\"400\">" +
                  "</canvas></article>";

var topHatsContent = "<h2>Bring Back the Top Hat!</h2><article>" +
                     "<img src=\"assets/tophat.jpg\">" +
                     "<p>Top hats have been around since the late 18th" +
                     " century, and were popular until the mid 20th" +
                     " century. They were originally made from beaver fur," +
                     " but this was later replaced by silk.</p>" +
                     "<p>Top hats are also a symbol of the magic community." +
                     " Producing a rabbit out of an empty top hat is one of" +
                     " the classics of magic. Hence the phrase 'hat" +
                     " trick'.</p>" +
                     "<p>Unfortunately, top hats are rarely seen in the 21st" +
                     " century. At the same time, magicians only rarely" +
                     " perform hat tricks. This is because magicians cater" +
                     " to their audiences; if there are no top hats, then" +
                     " there are no hat tricks.</p>" +
                     "<p>This game should provide all the hat-related magic" +
                     " you will ever need. However, it would be great if" +
                     " wearing top hats became commonplace again.</p>" +
                     "</article>";

var aboutContent = "<h2>About Me</h2><article>" +
                   "<p>Name: Lauren Gee<img src=\"assets/me.jpg\"></p>" +
                   "<p>Student number: 21495335</p>" +
                   "<p>Favourite colour: Purple</p>" +
                   "<p>Email: <a href=\"mailto:21495335@student.uwa.edu.au\">" +
                   "21495335@student.uwa.edu.au</a></p></article>" +

                   "<h2>Sources</h2><article><ul>" +
                   "<li>Game was made using the <a href=\"" +
                   "https://schteppe.github.io/p2.js/" +
                   "\">p2.js</a> physics engine</li>" +

                   "<li>Top hat image and information from: <a href=\"" +
                   "https://en.wikipedia.org/wiki/Top_hat\"" +
                   "\">https://en.wikipedia.org</a></li>" +

                   "<li>Background image from: <a href=\"" + 
                   "https://pixabay.com/en/damask-pattern-background-beige-937607/" +
                   "\">https://pixabay.com</a></li>" +
                   "</ul></article>";

// called when button pressed on navigation panel
function openInstructions() {
    if (page != INSTRUCTIONS) {
        if (page == GAME) {
            stopGame();
        }
        content.innerHTML = instructionsContent;
        page = INSTRUCTIONS;
    }
}

function openGame() {
    if (page != GAME) {
        content.innerHTML = gameContent;
        startGame();
        page = GAME;
    }
    // don't disrupt existing game
}

function openTopHats() {
    if (page != HAT) {
        if (page == GAME) {
            stopGame();
        }
        content.innerHTML = topHatsContent;
        page = HAT;
    }
}

function openAbout() {
    if (page != ABOUT) {
        if (page == GAME) {
            stopGame();
        }
        content.innerHTML = aboutContent;
        page = ABOUT;
    }
}

// runs when window loads
function init() {
    content = document.getElementById("content");
    openInstructions();
}
window.onload = init;

